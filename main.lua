local TILES = require "tiles"
local SPRITES = require "sprites"
local CAMERAS = require "cameras"

local info
local tileSheet
local tileQuads
local spriteTable
local cameraTable

function love.load()
  info = love.filesystem.load("maps/" .. arg[2] .. ".lua")()
  love.graphics.setBackgroundColor(info.backgroundcolor)

  tileSheet = love.graphics.newImage("maps/" .. info.tilesets[1].image)
  tileQuads = TILES.loadQuadList(info.tilesets[1])
  spriteTable = SPRITES.load(info.layers)
  cameraTable = CAMERAS.load(info.layers)
end

function love.update(dt)
  SPRITES.update(dt, spriteTable)
  CAMERAS.update(dt, cameraTable, info.tilewidth, info.tileheight)
end

function love.draw()
  CAMERAS.render(cameraTable, info.tilewidth, info.tileheight)

  for _, layer in pairs(info.layers) do
    if (layer.type == "tilelayer") then
      TILES.render(layer, tileSheet, tileQuads, info.tilewidth, info.tileheight)
    end
  end

  for _, sprite in pairs(spriteTable) do
    SPRITES.render(sprite, info.tilewidth, info.tileheight)
  end
end