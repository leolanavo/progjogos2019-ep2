###################
###   Alunos    ###
###################

1. Beatriz Marouelli - 9793652
2. Leonardo Lana - 9793735

###################
###   Módulos   ###
###################

1. cameras.lua

Movimentação da câmera.


2. sprites.lua

Todas as funções referentes aos sprites: cálculo de posição, renderização,
atualização da renderização (animação) e recorte dos sprites utilizados (quads).

3. tiles.lua

Todas as funções referentes aos tiles: cálculo de posição, renderização e 
recorte dos tiles utilizados (quads).

4. utils.lua

Implementação da tranformação linear para conversão da posição "tiles".
para tela. Usado por todos os outros módulos.

###################
###   Tarefas   ###
###################

Completamos quase todas as tarefas, exceto R4 (efeitos adicionais) 
e Q5 (tratar problemas de entrada). A tarefa R2 (renderizar os sprites)
foi parcialmente feita, pois o posicionamento está parcialmente incorreto
(próximo de onde a sprite deveria estar).

##################################
###   Comentários Adicionais   ###
##################################

O luacheck não acusa erros, mas acusa avisos sobre o "love" não estar
declarado.
