function love.conf(t)
  t.version = "11.2"
  t.window.title = "EP2 - Lana & Bia"

  t.window.width = 800
  t.window.height = 600
  t.window.vsync = 1
  t.window.display = 1

  t.modules.image = true
  t.modules.system = true
  t.modules.video = true
  t.modules.window = true
  t.modules.graphics = true
  t.modules.event = true
  t.modules.timer = true

  t.modules.audio = false
  t.modules.data = false
  t.modules.font = false
  t.modules.joystick = false
  t.modules.keyboard = false
  t.modules.mouse = false
  t.modules.physics = false
  t.modules.sound = false
  t.modules.thread = false
  t.modules.touch = false
  t.modules.math = false
end
