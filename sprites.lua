local UTILS = require "utils"

local SPRITES = {}

local function spritePosition(sprite, tilewidth, tileheight)
  local x = sprite.x/sprite.width
  local y = sprite.y/sprite.height

  return UTILS.linearTransform(x, y, sprite.z, tilewidth, tileheight)
end

local function loadFrames(frameList)
  local i, frames = 1, {}

  for frame_token in frameList:gmatch("%d+") do
    frames[i] = tonumber(frame_token)
    i = i + 1
  end

  return frames
end

local function loadSpriteQuad(sprite)
  local width, height = sprite.img:getDimensions()

  local spriteW =  width/sprite.properties.columns
  local spriteH = height/sprite.properties.rows

  local frame = sprite.frames[sprite.frameIndex] - 1

  local quad = love.graphics.newQuad(
    (frame % sprite.properties.columns) * spriteW,
    math.floor(frame / sprite.properties.columns) * spriteH,
    spriteW,
    spriteH,
    width,
    height
  )

  return quad
end

local function loadSprite(obj, layer)
  local sprite = obj
  sprite.img = love.graphics.newImage("chars/" .. obj.name .. ".png")
  sprite.timer = 1 / sprite.properties.fps
  sprite.z = layer.offsety
  sprite.frameIndex = 1
  sprite.frames = loadFrames(obj.properties.frames)
  sprite.quad = loadSpriteQuad(sprite)

  return sprite
end

function SPRITES.load(layers)
  local spriteTable = {}

  for _, layer in pairs(layers) do
    if (layer.type == "objectgroup") then
      for _, obj in pairs(layer.objects) do
        if (obj.type == "sprite") then
          spriteTable[obj.id] = loadSprite(obj, layer)
        end
      end
    end
  end
  return spriteTable
end

function SPRITES.render(sprite, tilewidth, tileheight)
  local x, y = spritePosition(sprite, tilewidth, tileheight)

  local sx = 1

  if (sprite.properties.flip) then
    sx = -1
  end

  love.graphics.draw(sprite.img, sprite.quad, x, y, 0, sx, 1)
end

function SPRITES.update(dt, spriteTable)
  for _, sprite in pairs(spriteTable) do
    sprite.timer = sprite.timer - dt
    if (sprite.timer <= 0) then
      local width, height  = sprite.img:getDimensions()
      local spriteW =  width/sprite.properties.columns
      local spriteH = height/sprite.properties.rows
      local frame = sprite.frames[sprite.frameIndex] - 1
      local x = (frame % sprite.properties.columns) * spriteW
      local y = math.floor(frame / sprite.properties.columns) * spriteH

      sprite.timer = 1 / sprite.properties.fps
      sprite.frameIndex =
        math.max(1, (sprite.frameIndex + 1) % (#sprite.frames + 1))
      sprite.quad:setViewport(x, y, spriteW, spriteH)
    end
  end
end

return SPRITES