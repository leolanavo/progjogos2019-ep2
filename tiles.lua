local UTILS = require "utils"

local TILES = {}

local function tilePosition(layer, index, tilewidth, tileheight)
  local z, columns = layer.offsety, layer.width
  local i = index - 1
  local x = i % columns
  local y = math.floor(i / columns)

  return UTILS.linearTransform(x, y, z, tilewidth, tileheight)
end

function TILES.loadQuadList(tileset)
  local quadList = {}
  local lines = tileset.imageheight/tileset.tileheight
  local columns = tileset.imagewidth/tileset.tilewidth

  for i = 1, lines do
    for j = 1, columns do
      local index = (i - 1) * columns + j
      local width = (j - 1)*tileset.tilewidth
      local height = (i - 1)*tileset.tileheight

      quadList[index] = love.graphics.newQuad(
        width,
        height,
        tileset.tilewidth,
        tileset.tileheight,
        tileset.imagewidth,
        tileset.imageheight
      )
    end
  end

  return quadList
end

function TILES.render(layer, tileSheet, quads, tilewidth, tileheight)
  for index, tile in pairs(layer.data) do
    if (tile ~= 0) then
      local x, y = tilePosition(layer, index, tilewidth, tileheight)
      love.graphics.draw(tileSheet, quads[tile], x, y)
    end
  end
end

return TILES