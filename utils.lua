local UTILS = {}

function UTILS.linearTransform(x, y, z, width, height)
  return
    (x - y) * width / 2,
    (x + y) * height / 2 + z
end

return UTILS