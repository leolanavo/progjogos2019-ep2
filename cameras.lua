local CAMERAS = {}

local UTILS = require "utils"

local function loadCamera(obj, layer)
  local camera = obj
  camera.z = layer.offsety
  return camera
end

local function cameraPosition(camx, camy, camz, camera, tilewidth, tileheight)
  local x = camx/camera.width
  local y = camy/camera.height

  return UTILS.linearTransform(x, y, camz, tilewidth, tileheight)
end

function CAMERAS.load(layers)
  local cameraTable = {
    index = 1,
    x = 0,
    y = 0,
    timer = 0,
    cams = {}
  }

  for _, layer in pairs(layers) do
    if (layer.type == "objectgroup") then
      for _, obj in pairs(layer.objects) do
        if (obj.type == "camera") then
          cameraTable.cams[tonumber(obj.name)] = loadCamera(obj, layer)
        end
      end
    end
  end

  cameraTable.x = cameraTable.cams[1].x
  cameraTable.y = cameraTable.cams[1].y
  cameraTable.z = cameraTable.cams[1].z

  return cameraTable
end

function CAMERAS.render(cameraTable, tilewidth, tileheight)
  local x, y = cameraPosition(
    cameraTable.x,
    cameraTable.y,
    cameraTable.z,
    cameraTable.cams[cameraTable.index],
    tilewidth,
    tileheight
  )
  love.graphics.translate(-x, -y)
end

function CAMERAS.update(dt, cameraTable, tilewidth, tileheight)
  local currCam = cameraTable.cams[cameraTable.index]
  local nextCam = cameraTable.cams[cameraTable.index + 1]

  if (not nextCam) then
    nextCam = cameraTable.cams[1]
  end

  cameraTable.timer = cameraTable.timer + dt
  local rate = math.min(1, cameraTable.timer/currCam.properties.duration)

  if (rate == 1) then
    cameraTable.timer = 0
    cameraTable.index =
      math.max(1, (cameraTable.index + 1) % (#cameraTable.cams + 1))
  end

  cameraTable.x = nextCam.x*rate + currCam.x*(1 - rate)
  cameraTable.y = nextCam.y*rate + currCam.y*(1 - rate)
  cameraTable.z = nextCam.z*rate + currCam.z*(1 - rate)

  CAMERAS.render(cameraTable, tilewidth, tileheight)
end

return CAMERAS